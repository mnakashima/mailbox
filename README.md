# Oberlo - Message API

This test was done with the following technologies:
- PHP 7
- Lumen

## Installation 
- Create a new database, rename the file .env.example to .env and update it with your database  configuration.
- Create a second database for phpunit and update the phpunit.xml file with your database configuration.

- Go to the project root folder and run:
```
# php composer install
# php artisan migrate
# php db:seed 
# php -S localhost:8000 ./public/
```
You should be able to access the application at http://localhost:8000

## API Documentation
Sample Message RESTful API

#### API Status Codes
- 200 - OK
- 401 - Unauthorized
- 400 - Bad Request
- 422 - Unprocessable Entity
- 500 - Internal Server Error

#### Authentication
This API requires Basic Authorization for all endpoints.
Username: api_user
Password: only_I_know

#### List messages 
Retrieve a paginateable list of all messages, having 15 messages per page.

**Endpoint:** GET http://{host}/message/all
**Parameters:**
- **page** (int) - page to be retrieved

**Request:**
GET http://{host}/message/all?page=2

**Output:**
```
{
  "total": 12,
  "per_page": 15,
  "current_page": 1,
  "last_page": 1,
  "next_page_url": null,
  "prev_page_url": null,
  "from": 1,
  "to": 12,
  "data": [
    {
      "uid": 1,
      "sender": "Ernest Hemingway",
      "subject": "animals",
      "message": "This is a tale about nihilism. The story is about a combative nuclear engineer who hates animals. It starts in a ghost town on a world of forbidden magic. The story begins with a legal dispute and ends with a holiday celebration.",
      "time_sent": 1459239867,
      "archived": 1,
      "read": 0,
      "created_at": "2016-12-14 04:11:46",
      "updated_at": "2016-12-14 04:11:46"
    }
}
```

#### List archived messages
Retrieve a paginateable list of all archived messages.

**Endpoint:** GET http://{host}/message/archived
**Parameters:**
- **page** (int) - page to be retrieved

**Request:**
GET http://{host}/message/archived?page=2

**Output:**
```
{
  "total": 12,
  "per_page": 15,
  "current_page": 1,
  "last_page": 1,
  "next_page_url": null,
  "prev_page_url": null,
  "from": 1,
  "to": 12,
  "data": [
    {
      "uid": 1,
      "sender": "Ernest Hemingway",
      "subject": "animals",
      "message": "This is a tale about nihilism. The story is about a combative nuclear engineer who hates animals. It starts in a ghost town on a world of forbidden magic. The story begins with a legal dispute and ends with a holiday celebration.",
      "time_sent": 1459239867,
      "archived": 1,
      "read": 0,
      "created_at": "2016-12-14 04:11:46",
      "updated_at": "2016-12-14 04:11:46"
    }
}
```
#### Show message
Retrieve message by id.

**Endpoint:** GET http://{host}/message/{id}
**Parameter:**
- **id** (int) - The message id

**Request:**
GET http://{host}/message/2
```
**Output:**
{
  "uid": 2,
  "sender": "Stephen King",
  "subject": "adoration",
  "message": "The story is about a fire fighter, a naive bowman, a greedy fisherman, and a clerk who is constantly opposed by a heroine. It takes place in a small city. The critical element of the story is an adoration.",
  "time_sent": 1459248747,
  "archived": 1,
  "read": 0,
  "created_at": "2016-12-14 04:15:56",
  "updated_at": "2016-12-14 04:15:56"
}
```
#### Read message
This action “reads” a message and marks it as read in database.

**Endpoint:** POST http://{host}/message/read/{id}
**Parameters:**
- **id** (int) - The message id

**Request:**
POST http://{host}/message/read/2

**Output:**
```
{
  "uid": 2,
  "sender": "Stephen King",
  "subject": "adoration",
  "message": "The story is about a fire fighter, a naive bowman, a greedy fisherman, and a clerk who is constantly opposed by a heroine. It takes place in a small city. The critical element of the story is an adoration.",
  "time_sent": 1459248747,
  "archived": 0,
  "read": 1,
  "created_at": "2016-12-14 04:15:56",
  "updated_at": "2016-12-14 04:15:56"
}
```
#### Archive message
This action sets a message to archived. 

**Endpoint:** POST http://{host}/message/archive/{id}
**Parameters:**
- **id** (int) - The message id

**Request:**
POST http://{host}/message/archive/2

```
{
  "uid": 2,
  "sender": "Stephen King",
  "subject": "adoration",
  "message": "The story is about a fire fighter, a naive bowman, a greedy fisherman, and a clerk who is constantly opposed by a heroine. It takes place in a small city. The critical element of the story is an adoration.",
  "time_sent": 1459248747,
  "archived": 1,
  "read": 0,
  "created_at": "2016-12-14 04:15:56",
  "updated_at": "2016-12-14 04:15:56"
}
```

### Testing
Go to the project root folder and run:
```
# phpunit
```
or
```
vendor/phpunit/phpunit/phpunit
```

You can also import the postman collection file, located in docs folder, to [POSTMAN](https://www.getpostman.com/) to manually run your tests.


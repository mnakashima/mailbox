<?php

namespace App\Http\Controllers;

use Oberlo\Message\MessageManager;
use Illuminate\Http\Request;
use Oberlo\Common\Validate\ArrayValidate;

/**
 * Class MessageController
 *
 * @package App\Http\Controllers
 */
class MessageController extends Controller
{
    use ArrayValidate;

    private $messageManager;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->messageManager = new MessageManager();
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function all(Request $request)
    {
        $this->validateArray($request->all(),[
            'page' => 'int'
        ]);

        $result = $this->messageManager->all();

        return response()->json($result);

    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function archived(Request $request)
    {
        $this->validateArray($request->all(),[
            'page' => 'int'
        ]);

        $response = $this->messageManager->archived();

        return response()->json($response);
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $this->validateArray(['id' => $id],[
            'id' => 'int'
        ]);

        $message = $this->messageManager->show($id);
        return response()->json($message);
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function read($id)
    {
        $this->validateArray(['id' => $id],[
            'id' => 'int'
        ]);

        $message = $this->messageManager->read($id);
        return response()->json($message);
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function archive($id)
    {
        $this->validateArray(['id' => $id],[
            'id' => 'int'
        ]);

        $message = $this->messageManager->archive($id);
        return response()->json($message);
    }
}

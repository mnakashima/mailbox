<?php

namespace App\Http\Middleware;

use Closure;


/**
 * Class BasicAuthMiddleware
 *
 * @package App\Http\Middleware
 */
class BasicAuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        if($request->getUser() != 'api_user' || $request->getPassword() != 'only_I_know') {
            $headers = array('WWW-Authenticate' => 'Basic');
            return response("Unauthorized request", 401, $headers);
        }
        return $next($request);
    }
}
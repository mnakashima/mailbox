<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
    return $app->version();
});
$app->post('/authenticate', 'AuthController@authenticate');

$app->group(['middleware' => 'BasicAuth'], function () use ($app) {
    $app->get('message/all','MessageController@all');
    $app->get('message/archived','MessageController@archived');
    $app->get('message/{id}','MessageController@show');
    $app->post('message/read/{id}','MessageController@read');
    $app->post('message/archive/{id}','MessageController@archive');
});

<?php
/**
 * Created by PhpStorm.
 * User: nakashima
 * Date: 12/12/16
 * Time: 23:05
 */

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Message
 *
 * @package App
 */
class Message extends Model
{
    /**
     * @var string
     */
    protected $table = "message";

    /**
     * @var string
     */
    protected $primaryKey = 'uid';

    protected $fillable = ['sender', 'subject', 'message', 'time_sent', 'archived', 'read'];
}
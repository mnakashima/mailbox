<?php

namespace Oberlo\Common;

use \stdClass;

/**
 * Class Cache
 *
 * @package Oberlo\Contracts
 */
class Cache
{
    /**
     * @var null
     */
    private $driver = null;

    /**
     * @param \stdClass $driver
     */
    public static function driver(stdClass $driver)
    {
        self::driver($driver);
    }

    /**
     * @param $name
     *
     * @return mixed
     */
    public static function get($name)
    {
        return self::$driver->get($name);
    }

    /**
     * @param $name
     * @param $value
     * @param int $timeout
     */
    public static function set($name, $value, $timeout = 60)
    {
        self::$driver->put($name, $value, $timeout);
    }
}
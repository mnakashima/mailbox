<?php

namespace Oberlo\Common\Entity;

use \Oberlo\Common\Entity\Entity;
use \JsonSerializable;

/**
 * Class BaseEntity
 *
 * @package Oberlo\Common
 */
class BaseEntity implements Entity, JsonSerializable
{
    /**
     * @var array
     */
    private $attributes = array();

    /**
     * BaseEntity constructor.
     */
    public function __construct()
    {
    }

    /**
     * @param $name
     *
     * @return mixed|null
     */
    public function __get($name)
    {
        $return = null;
        if (isset($this->attributes[$name])) {
            $return = $this->attributes[$name];
        }

        return $return;
    }

    /**
     * @param $name
     * @param $value
     */
    public function __set($name, $value)
    {
        $this->attributes[$name] = $value;
    }

    /**
     * @param array $attributes
     */
    public function fill(array $attributes)
    {
        $this->attributes = $attributes;
    }

    /**
     * @return array
     */
    public function attributes() : array
    {
        return $this->attributes;
    }

    /**
     * @return array
     */
    public function jsonSerialize() : array
    {
        return $this->attributes;
    }
}
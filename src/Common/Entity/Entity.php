<?php

namespace Oberlo\Common\Entity;

/**
 * Interface Entity
 */
interface Entity
{
    /**
     * @return mixed
     */
    public function attributes();

    /**
     * @param $name
     *
     * @return mixed
     */
    public function __get($name);

    /**
     * @param $name
     * @param $value
     *
     * @return void
     */
    public function __set($name, $value);

    /**
     * @param array $attributes
     *
     * @return void
     *
     */
    public function fill(array $attributes);
}
<?php

namespace Oberlo\Common\Entity;

use \Oberlo\Common\Entity\BaseEntity;

/**
 * Class MessageEntity
 *
 * @package Oberlo\Common
 */
class MessageEntity extends BaseEntity
{
    /**
     * @property int uid
     * @property string sender
     * @property string subject
     * @property string message
     * @property int time_sent
     * @property bool archived
     */
}
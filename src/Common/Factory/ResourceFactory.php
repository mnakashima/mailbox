<?php

namespace Oberlo\Common\Factory;

use Illuminate\Database\Eloquent\Model;
use Oberlo\Common\BaseEntity;
use \App\Model\Message;
use Oberlo\Common\Entity\Entity;
use Symfony\Component\Translation\Exception\NotFoundResourceException;

/**
 * Class ResourceFactory
 *
 * @package Oberlo\Common
 */
class ResourceFactory
{
    /**
     * @param string $entityName
     *
     * @throws NotFoundResourceException - Entity class {$entityClass} not found - 001
     *
     * @return \Oberlo\Common\BaseEntity
     */
    public static function createEntityInstance(string $entityName) : Entity
    {

        $namespace = env("MODEL_NAMESPACE", "\\Oberlo\\Common\\Entity\\");
        $entityClass = $namespace . $entityName . "Entity";

        if (!class_exists($entityClass)) {
            throw new NotFoundResourceException("Entity class {$entityClass} not found", 001);
        }

        return new $entityClass();
    }

    /**
     * @param string $modelName
     *
     * @throws NotFoundResourceException - Model class {$modelClass} not found - 002
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public static function createModelInstance(string $modelName) : Model
    {
        $namespace = env("MODEL_NAMESPACE", "\\App\\Model\\");
        $modelClass = $namespace . $modelName;

        if (!class_exists($modelClass)) {
            throw new NotFoundResourceException("Model class {$modelClass} not found", 001);
        }

        return new $modelClass();
    }

    /**
     * @param string $entityName
     * @param \Illuminate\Database\Eloquent\Model $model
     *
     * @return \Oberlo\Common\BaseEntity|\Oberlo\Common\Entity\Entity
     */
    public static function createPopulatedInstanceFromModel(string $entityName,Model $model)
    {
        $entity = self::createEntityInstance($entityName);
        $entity->fill($model->getAttributes());
        return $entity;
    }
}
<?php

namespace Oberlo\Common\Repository;

use Oberlo\Common\Entity\BaseEntity;
use Oberlo\Common\Entity\Entity;
use Oberlo\Common\Factory\ResourceFactory;
use Symfony\Component\Translation\Exception\NotFoundResourceException;

/**
 * Class BaseRepository
 *
 * @package Oberlo\Common
 */
class BaseRepository implements Repository
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var \Illuminate\Database\Eloquent\Model
     */
    protected $model;

    /**
     * @var bool
     */
    protected $paginate = false;

    /**
     * @var int
     */
    protected $perPage;

    /**
     * BaseRepository constructor.
     *
     * @throws NotFoundResourceException - Model class {$modelClass} not found - 002
     *
     */
    public function __construct()
    {
        $this->model = ResourceFactory::createModelInstance($this->name);
    }

    /**
     * @param int $perPage
     *
     * @return $this
     */
    public function paginate($perPage = 15)
    {
        $this->paginate = true;
        $this->perPage = $perPage;
        return $this;
    }

    /**
     * @param int $id
     *
     * @return BaseEntity
     */
    public function find(int $id)
    {
        return $this->findByAttributes([$this->model->getKeyName() => $id]);
    }

    /**
     * @param int $paginate
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function findAll()
    {
        $result = [];
        if($this->paginate) {
            $result = $this->model->paginate($this->perPage);
        } else {
            $all = $this->model->all();
            foreach($all as $model) {
                $result[] = ResourceFactory::createPopulatedInstanceFromModel($this->name,$model);
            }
        }

        $this->paginate = false;

        return $result;
    }

    /**
     * @param array $param
     *
     * @throws NotFoundResourceException - Entity class {$entityClass} not found - 001
     *
     * @return mixed
     */
    public function findByAttributes(array $param)
    {
        $return = null;
        $model = $this->model;
        foreach ($param as $key => $val) {
            $model = $model->where($key,'=',$val);
        }

        $result = $model->limit(1)->first();

        if($result) {
            $return = ResourceFactory::createPopulatedInstanceFromModel($this->name,$result);
        }

        return $return;
    }

    /**
     * @param array $param
     * @param int $limit
     * @param bool $paginate
     *
     * @return array
     */
    public function findAllByAttributes(array $param)
    {
        $return = [];

        foreach ($param as $key => $val) {
            $this->model = $this->model->where($key,'=',$val);
        }

        if($this->paginate) {
            $return = $this->model->paginate($this->perPage);

        } else {
            $all = $this->model->get();
            foreach($all as $model) {
                $return[] = ResourceFactory::createPopulatedInstanceFromModel($this->name,$model);
            }
        }

        return $return;
    }

    /**
     * @param \Oberlo\Common\Entity\Entity $entity
     *
     * @return bool
     */
    public function save(Entity $entity): bool
    {
        $pk = $this->model->getKeyName();
        $element = $this->model->find($entity->{$pk});
        $element->fill($entity->attributes());
        return $element->save();
    }
}
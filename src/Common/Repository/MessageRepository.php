<?php

namespace Oberlo\Common\Repository;

/**
 * Class MessageRepository
 *
 * @package Oberlo\Common
 */
class MessageRepository extends BaseRepository
{
    /**
     * @var string
     */
    protected $name = "Message";
}
<?php

namespace Oberlo\Common\Repository;

use \Oberlo\Common\Entity\Entity;

/**
 * Interface Repository
 *
 * @package Oberlo\Common
 */
interface Repository
{
    /**
     * @param int $id
     *
     * @return mixed
     */
    public function find(int $id);

    /**
     * @return $this
     */
    public function paginate();
    /**
     * @param array $param
     *
     * @return mixed
     */
    public function findByAttributes(array $param);

    /**
     * @param array $param
     * @param int $paginate
     *
     * @return array
     */
    public function findAllByAttributes(array $param);

    /**
     * @param \Oberlo\Common\BaseEntity $entity
     *
     * @return bool
     */
    public function save(Entity $entity): bool;
}
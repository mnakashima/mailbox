<?php

namespace Oberlo\Common\Validate;

use Laravel\Lumen\Routing\ProvidesConvenienceMethods;
use Illuminate\Http\Request;

/**
 * Class ArrayValidate
 *
 * @package Oberlo\Common\Validate
 */
trait ArrayValidate
{
    use ProvidesConvenienceMethods;

    /**
     * @param array $data
     * @param array $rules
     * @param array $messages
     * @param array $customAttributes
     */
    public function validateArray(array $data, array $rules, array $messages = [], array $customAttributes = [])
    {
        $request = new Request();
        $request->merge($data);

        $validator = $this->getValidationFactory()->make($request->all(), $rules, $messages, $customAttributes);

        if ($validator->fails()) {
            $this->throwValidationException($request, $validator);
        }
    }
}
<?php

namespace Oberlo\Message;

use Oberlo\Common\Exception\OberloException;
use Oberlo\Common\Repository\MessageRepository;
use Oberlo\Common\Entity\MessageEntity;

/**
 * Class MessageManager
 *
 * @package Oberlo\Message
 */
class MessageManager
{
    /**
     * @var MessageRepository
     */
    private $messageRepository;

    /**
     * MessageManager constructor.
     */
    public function __construct()
    {
        $this->messageRepository = new MessageRepository();
    }


    /**
     * @param int $id
     *
     * @return \Oberlo\Common\Entity\MessageEntity
     */
    public function show(int $id)
    {
        return $this->messageRepository->find($id);
    }

    /**
     * @param array $offset
     *
     * @return array
     */
    public function all()
    {
        return $this->messageRepository->paginate()->findAll();
    }

    /**
     * @param array $offset
     *
     * @return array
     */
    public function archived()
    {
        return $this->messageRepository->paginate()->findAllByAttributes(['archived' => true]);
    }

    /**
     * @param int $id
     *
     * @return \Oberlo\Common\Entity\MessageEntity
     *
     * @throws OberloException - Message id {$id} not found - 003
     *
     * @throws OberloException - Fail to update as archived message Id {$id} - 004
     */
    public function archive(int $id) : MessageEntity
    {
        $message = $this->messageRepository->find($id);

        if (!$message) {
            throw new OberloException("Message id {$id} not found", 003);
        }

        $message->archived = 1;

        $save = $this->messageRepository->save($message);

        if (!$save) {
            throw new OberloException("Fail to update as archived message Id {$id} ", 004);
        }

        return $message;
    }

    /**
     * @param int $id
     *
     * @return \Oberlo\Common\Entity\MessageEntity
     *
     * @throws OberloException - Message id {$id} not found - 005
     *
     * @throws OberloException - Fail to update as read message Id {$id} - 006
     */
    public function read(int $id) : MessageEntity
    {
        $message = $this->messageRepository->find($id);

        if (!$message) {
            throw new OberloException("Message id {$id} not found", 005);
        }

        $message->read = 1;
        $save = $this->messageRepository->save($message);


        if (!$save) {
            throw new OberloException("Fail to update as read message Id {$id} ", 006);
        }

        return $message;
    }
}
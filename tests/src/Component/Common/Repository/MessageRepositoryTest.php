<?php

use Oberlo\Common\Repository\MessageRepository;
use Oberlo\Common\Entity\MessageEntity;

/**
 * Class MessageRepositoryTest
 */
class MessageRepositoryTest extends TestCase
{
    public function testFind()
    {
        $fakeMessage = factory('App\Model\Message')->make();
        $fakeMessage->save();

        $repository = new MessageRepository();
        $message = $repository->find($fakeMessage->uid);

        $this->assertInstanceOf(MessageEntity::class,$message);
        $this->assertEquals($message->uid,$fakeMessage->uid);
    }

    public function testFindByAttributes()
    {
        $fakeMessage = factory('App\Model\Message')->make();
        $fakeMessage->save();

        $repository = new MessageRepository();
        $message = $repository->findByAttributes([
            'uid' => $fakeMessage->uid,
            'sender' => $fakeMessage->sender
        ]);

        $this->assertInstanceOf(MessageEntity::class,$message);
        $this->assertEquals($message->uid,$fakeMessage->uid);
    }

    public function testFindAllByAttributes()
    {
        $fakeMessage = factory('App\Model\Message')->make(
            ['sender' => 'fakeSender']
        );
        $fakeMessage->save();

        $fakeMessage = factory('App\Model\Message')->make(
            ['sender' => 'fakeSender']
        );
        $fakeMessage->save();

        $repository = new MessageRepository();
        $result = $repository->findAllByAttributes([
            'sender' => 'fakeSender'
        ]);

        $this->assertNotEmpty($result);
        $this->assertCount(2,$result);
        $this->assertInstanceOf(MessageEntity::class,end($result));
        $this->assertEquals($fakeMessage->sender,end($result)->sender);
    }
}
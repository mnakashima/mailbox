<?php

use Oberlo\Message\MessageManager;
use Oberlo\Common\Entity\MessageEntity;

class MessageManagerTest extends TestCase
{

    public function testShow()
    {
        $fakeMessage = factory('App\Model\Message')->make();
        $fakeMessage->save();

        $manager = new MessageManager();
        $message = $manager->show($fakeMessage->uid);

        $this->assertInstanceOf(MessageEntity::class,$message);
        $this->assertEquals($fakeMessage->uid,$message->uid);
    }

    public function testAll()
    {
        $fakeMessage = factory('App\Model\Message')->make();
        $fakeMessage->save();
        $fakeMessage = factory('App\Model\Message')->make();
        $fakeMessage->save();

        $manager = new MessageManager();
        $result = $manager->all();

        $this->assertCount(2,$result);
    }

    public function testArchived()
    {
        $fakeMessage = factory('App\Model\Message')->make();
        $fakeMessage->save();

        $this->assertFalse($fakeMessage->archived);

        $fakeMessage = factory('App\Model\Message')->make(['archived' => true]);
        $fakeMessage->save();
        $fakeMessage = factory('App\Model\Message')->make(['archived' => true]);
        $fakeMessage->save();

        $this->assertTrue($fakeMessage->archived);

        $manager = new MessageManager();
        $result = $manager->archived();

        $this->assertCount(2,$result);

    }

    public function testArchive()
    {
        $fakeMessage = factory('App\Model\Message')->make();
        $fakeMessage->save();

        $this->assertFalse($fakeMessage->archived);

        $manager = new MessageManager();
        $message = $manager->archive($fakeMessage->uid);

        $this->assertEquals($fakeMessage->uid,$message->uid);

        $message = $manager->show($message->uid);

        $this->assertInstanceOf(MessageEntity::class,$message);
        $this->assertEquals(true,$message->archived);
    }

    public function testRead()
    {
        $fakeMessage = factory('App\Model\Message')->make();
        $fakeMessage->save();

        $this->assertFalse($fakeMessage->read);

        $manager = new MessageManager();
        $message = $manager->read($fakeMessage->uid);

        $this->assertEquals($fakeMessage->uid,$message->uid);

        $message = $manager->show($message->uid);

        $this->assertInstanceOf(MessageEntity::class,$message);
        $this->assertEquals(true,$message->read);
    }

    /**
     * @expectedException \Oberlo\Common\Exception\OberloException
     * @expectedExceptionCode 003
     */
    public function testArchiveMessageNotFoundOberloException()
    {
        $manager = new MessageManager();
        $manager->archive(999);
    }

    /**
     * @expectedException \Oberlo\Common\Exception\OberloException
     * @expectedExceptionCode 005
     */
    public function testReadMessageNotFoundOberloException()
    {
        $manager = new MessageManager();
        $manager->read(999);
    }
}
<?php

class MessageEndPointTest extends TestCase
{
    public function jsonRequest($verb,$url,$params = [])
    {
        return $this->json($verb,$url,$params,[
            'Authorization' => 'Basic YXBpX3VzZXI6b25seV9JX2tub3c='
        ]);
    }

    public function testAll()
    {

        $fakeMessage = factory('App\Model\Message')->make();
        $fakeMessage->save();

        $response = $this->jsonRequest('get','/message/all',[]);
        $response->seeStatusCode(200);
        $response->seeJsonStructure(['total','per_page','current_page','data']);
        $response->seeJson(['total' => 1,'uid' => $fakeMessage->uid]);

        $response = $this->jsonRequest('get','/message/all',['page' => 2]);
        $response->seeStatusCode(200);
        $response->seeJson(['current_page' => 2]);

        $response = $this->jsonRequest('get','/message/all',['page' => 'page']);
        $response->seeStatusCode(422);
    }

    public function testArchived()
    {

        $fakeMessage = factory('App\Model\Message')->make();
        $fakeMessage->save();

        $response = $this->jsonRequest('get','/message/archived');
        $response->seeStatusCode(200);
        $response->seeJsonStructure(['total','per_page','current_page','data']);
        $response->seeJson(['total' => 0]);

        $fakeMessage = factory('App\Model\Message')->make(['archived' => true]);
        $fakeMessage->save();

        $fakeMessage = factory('App\Model\Message')->make(['archived' => true]);
        $fakeMessage->save();

        $response = $this->jsonRequest('get','/message/archived');
        $response->seeStatusCode(200);
        $response->seeJsonStructure(['total','per_page','current_page','data']);
        $response->seeJson(['total' => 2]);
        $response->seeJson(['uid' => $fakeMessage->uid]);


        $response = $this->jsonRequest('get','/message/archived',['page' => 2]);
        $response->seeStatusCode(200);
        $response->seeJson(['current_page' => 2]);

        $response = $this->jsonRequest('get','/message/archived',['page' => 'page']);
        $response->seeStatusCode(422);
    }

    public function testShow()
    {
        $fakeMessage = factory('App\Model\Message')->make();
        $fakeMessage->save();

        $response = $this->jsonRequest('get','/message/'.$fakeMessage->uid);
        $response->seeStatusCode(200);
        $response->seeJson(["uid" => $fakeMessage->uid]);

        $response = $this->jsonRequest('get','/message/99999');
        $response->seeStatusCode(200);

        $response = $this->jsonRequest('get','/message/abc');
        $response->seeStatusCode(422);
    }

    public function testArchive()
    {
        $fakeMessage = factory('App\Model\Message')->make();
        $fakeMessage->save();

        $response = $this->jsonRequest('post','/message/archive/'.$fakeMessage->uid);
        $response->seeStatusCode(200);
        $response->seeJson(['archived'=> 1]);
        $response->seeJson(['uid'=> $fakeMessage->uid]);

        $response = $this->jsonRequest('post','/message/archive/abc');
        $response->seeStatusCode(422);

        $response = $this->jsonRequest('post','/message/archive/99999');
        $response->seeStatusCode(400);
        $response->seeJson(["code" => 3]);
    }

    public function testRead()
    {
        $fakeMessage = factory('App\Model\Message')->make();
        $fakeMessage->save();

        $response = $this->jsonRequest('post','/message/read/'.$fakeMessage->uid);
        $response->seeStatusCode(200);
        $response->seeJson(['read'=> 1]);
        $response->seeJson(['uid'=> $fakeMessage->uid]);

        $response = $this->jsonRequest('post','/message/read/abc');
        $response->seeStatusCode(422);

        $response = $this->jsonRequest('post','/message/read/9999');
        $response->seeStatusCode(400);
        $response->seeJson(["code" => 5]);
    }
}